package com.globalbserve.connector.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.globalbserve.connector.entities.*;

@Repository
public interface TemplateDao extends JpaRepository<TemplateConfig, Long> {

}
