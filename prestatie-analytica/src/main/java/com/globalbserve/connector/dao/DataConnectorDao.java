package com.globalbserve.connector.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.globalbserve.connector.entities.ConnectionDetail;

public interface DataConnectorDao extends JpaRepository<ConnectionDetail, Long>{

}
