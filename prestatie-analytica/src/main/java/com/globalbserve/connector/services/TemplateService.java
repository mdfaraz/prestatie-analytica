package com.globalbserve.connector.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalbserve.connector.dao.TemplateDao;
import com.globalbserve.connector.entities.TemplateConfig;

@Service
public class TemplateService {
	
	@Autowired
	private TemplateDao templateDao;
	
	public void createTemplate(TemplateConfig config) {
		templateDao.save(config);
	}

	public List<TemplateConfig> getAllTemplates() {
		return templateDao.findAll();
	}

	public Optional<TemplateConfig> getTemplate(long id) {
		// TODO Auto-generated method stub
		return templateDao.findById(id);
	}

}
