package com.globalbserve.connector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalbserve.connector.dao.DataConnectorDao;
import com.globalbserve.connector.entities.ConnectionDetail;

@Service
public class DataConnectorService {
	
	@Autowired
	private DataConnectorDao connectorDao;
	
	public boolean initiateDbLoad(ConnectionDetail connectionDetail) {
		connectorDao.save(connectionDetail);
		return false;
	}

}
