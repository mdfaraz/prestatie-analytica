package com.globalbserve.connector.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.globalbserve.connector.dao.TemplateDao;
import com.globalbserve.connector.entities.TemplateConfig;
import com.globalbserve.connector.services.TemplateService;

@RestController
@RequestMapping("/db/template")
public class TemplateController {
	
	@Autowired
	private TemplateService templateService;
	
	@PostMapping("/create")
	public void createTemplate(@RequestBody TemplateConfig config) {
		if(config == null)
		{
			return;
		}
		
		templateService.createTemplate(config);
	}
	
	
	@GetMapping("/all")
	public List<TemplateConfig> getAllTemplates(){
		return templateService.getAllTemplates();
	}
	
	
	@GetMapping("/template/{id}")
	public TemplateConfig getTemplate(@RequestParam("id") long id) {
		//return templateService.getTemplate(Id);
		Optional<TemplateConfig> optional = templateService.getTemplate(id);
		return optional.get();
	}

}
