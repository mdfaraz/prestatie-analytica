package com.globalbserve.connector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globalbserve.connector.entities.ConnectionDetail;
import com.globalbserve.connector.entities.ConnectionResp;
import com.globalbserve.connector.entities.ErrorResp;
import com.globalbserve.connector.services.DataConnectorService;

import static org.apache.commons.lang3.StringUtils.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@RestController
@RequestMapping("/db/data")
public class DataController {
	
	Log log = LogFactory.getLog(DataController.class);
	
	@Autowired
	private DataConnectorService service;
	
	@PostMapping("/initiate")
	public ConnectionResp initiateDbLoad(@RequestBody ConnectionDetail detail) {
		
		if(detail == null) {
			log.warn("invalid request.");
			return new ConnectionResp(new ErrorResp("DC_IR", "Invalid Request" ));
		}
		
		if(isBlank(detail.getSrcHostName())) {
			log.warn("invalid Source host name provided.");
			return new ConnectionResp(new ErrorResp("DC_ISH", "Invalid Source Host name provided" ));
		}
		
		if(isBlank(detail.getSrcPort())) {
			log.warn("invalid Source port provided.");
			return new ConnectionResp(new ErrorResp("DC_ISP", "Invalid Source Port provided" ));
		}
		
		if(isBlank(detail.getDestHostName())) {
			log.warn("invalid Destination host name provided.");
			return new ConnectionResp(new ErrorResp("DC_IDH", "Invalid Destination Host name provided" ));
		}
		
		if(isBlank(detail.getDestPort())) {
			log.warn("invalid Destination Port name provided.");
			return new ConnectionResp(new ErrorResp("DC_IDP", "Invalid Destination Port provided" ));
		}
		
		final boolean accepted = service.initiateDbLoad(detail);
		return new ConnectionResp(accepted , null);
	}

}
