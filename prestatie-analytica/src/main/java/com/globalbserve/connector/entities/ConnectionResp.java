package com.globalbserve.connector.entities;

public class ConnectionResp {
	
	private boolean accepted;
	private ErrorResp error;
	
	public ConnectionResp(ErrorResp error) {
		super();
		this.error = error;
	}

	public ConnectionResp(boolean accepted, ErrorResp error) {
		super();
		this.accepted = accepted;
		this.error = error;
	}
	
	
	public boolean isAccepted() {
		return accepted;
	}
	public ErrorResp getError() {
		return error;
	}
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	public void setError(ErrorResp error) {
		this.error = error;
	}
	@Override
	public String toString() {
		return "ConnectionResp [accepted=" + accepted + ", error=" + error + "]";
	}
}
