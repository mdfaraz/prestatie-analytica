package com.globalbserve.connector.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TEMPLATE_CONFIG",catalog="connector")
public class TemplateConfig {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="SOURCE_DB_TYPE",nullable = false)
	private String sourceDBType;
	
	@Column(name="DESTINATION_DB_TYPE",nullable = false)
	private String destinationDbType;
	
	@Column(name="SOURCE_DRIVER",nullable = false)
	private String sourceDriver;
	
	@Column(name="DESTINATION_DRIVER",nullable = false)
	private String destinationDriver;

	public long getId() {
		return id;
	}

	public String getSourceDBType() {
		return sourceDBType;
	}

	public String getDestinationDbType() {
		return destinationDbType;
	}

	public String getSourceDriver() {
		return sourceDriver;
	}

	public String getDestinationDriver() {
		return destinationDriver;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSourceDBType(String sourceDBType) {
		this.sourceDBType = sourceDBType;
	}

	public void setDestinationDbType(String destinationDbType) {
		this.destinationDbType = destinationDbType;
	}

	public void setSourceDriver(String sourceDriver) {
		this.sourceDriver = sourceDriver;
	}

	public void setDestinationDriver(String destinationDriver) {
		this.destinationDriver = destinationDriver;
	}

	@Override
	public String toString() {
		return "TemplateConfig [id=" + id + ", sourceDBType=" + sourceDBType + ", destinationDbType="
				+ destinationDbType + ", sourceDriver=" + sourceDriver + ", destinationDriver=" + destinationDriver
				+ "]";
	}
	
}
