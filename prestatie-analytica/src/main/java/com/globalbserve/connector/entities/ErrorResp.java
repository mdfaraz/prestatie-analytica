package com.globalbserve.connector.entities;

public class ErrorResp {
	
	private String errorCode;
	private String errorDesc;
	
	public ErrorResp(String errorCode, String errorDesc) {
		super();
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	@Override
	public String toString() {
		return "ErrorResp [errorCode=" + errorCode + ", errorDesc=" + errorDesc + "]";
	}

}
