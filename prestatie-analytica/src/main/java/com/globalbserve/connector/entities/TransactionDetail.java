package com.globalbserve.connector.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class TransactionDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="CONNECTION_ID",nullable = false)
	private String connectionId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRANSACTION_ST_DATE")
	private Date transactionstartDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRANSACTION_END_DATE")
	private Date transactionEndDate;
	
	@Column(name="RECORDS_NUMBER")
	private int noOfRecordsInserted;
	
	@Column(name="IN_FLIGHT")
	private boolean inFlight;
	
	@Column(name="IS_PROCESSED")
	private boolean isProcessed;
	
	@Column(name="IS_SUCCESS")
	private boolean success;
	
	@Column(name="ERROR_DETAIL")
	private String errorDetail;

	public long getId() {
		return id;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public Date getTransactionstartDate() {
		return transactionstartDate;
	}

	public Date getTransactionEndDate() {
		return transactionEndDate;
	}

	public int getNoOfRecordsInserted() {
		return noOfRecordsInserted;
	}

	public boolean isInFlight() {
		return inFlight;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public void setTransactionstartDate(Date transactionstartDate) {
		this.transactionstartDate = transactionstartDate;
	}

	public void setTransactionEndDate(Date transactionEndDate) {
		this.transactionEndDate = transactionEndDate;
	}

	public void setNoOfRecordsInserted(int noOfRecordsInserted) {
		this.noOfRecordsInserted = noOfRecordsInserted;
	}

	public void setInFlight(boolean inFlight) {
		this.inFlight = inFlight;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	@Override
	public String toString() {
		return "TransactionDetail [id=" + id + ", connectionId=" + connectionId + ", transactionstartDate="
				+ transactionstartDate + ", transactionEndDate=" + transactionEndDate + ", noOfRecordsInserted="
				+ noOfRecordsInserted + ", inFlight=" + inFlight + ", isProcessed=" + isProcessed + "]";
	}
	
	
}
