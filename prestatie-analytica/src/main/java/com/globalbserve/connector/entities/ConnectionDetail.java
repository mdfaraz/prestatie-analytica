package com.globalbserve.connector.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CONNECTION_DETAILS",catalog="connector")
public class ConnectionDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="TEMPLATE_ID",nullable = false)
	private String templateId;
	
	@Column(name="SRC_HOST_NAME",nullable = false)
	private String srcHostName;
	
	@Column(name="SRC_PORT",nullable = false)
	private String srcPort;
	
	@Column(name="SRC_DB_NAME")
	private String srcDbName;
	
	@Column(name="DEST_HOST_NAME",nullable = false)
	private String destHostName;
	
	@Column(name="DEST_PORT",nullable = false)
	private String destPort;
	
	@Column(name="DEST_DB_NAME")
	private String destDbName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONNECTION_DATE")
	private Date connectionDate;

	public long getId() {
		return id;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getSrcHostName() {
		return srcHostName;
	}

	public String getSrcPort() {
		return srcPort;
	}

	public String getSrcDbName() {
		return srcDbName;
	}

	public String getDestHostName() {
		return destHostName;
	}

	public String getDestPort() {
		return destPort;
	}

	public String getDestDbName() {
		return destDbName;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public void setSrcHostName(String srcHostName) {
		this.srcHostName = srcHostName;
	}

	public void setSrcPort(String srcPort) {
		this.srcPort = srcPort;
	}

	public void setSrcDbName(String srcDbName) {
		this.srcDbName = srcDbName;
	}

	public void setDestHostName(String destHostName) {
		this.destHostName = destHostName;
	}

	public void setDestPort(String destPort) {
		this.destPort = destPort;
	}

	public void setDestDbName(String destDbName) {
		this.destDbName = destDbName;
	}

	@Override
	public String toString() {
		return "ConnectionDetail [id=" + id + ", templateId=" + templateId + ", srcHostName=" + srcHostName
				+ ", srcPort=" + srcPort + ", srcDbName=" + srcDbName + ", destHostName=" + destHostName + ", destPort="
				+ destPort + ", destDbName=" + destDbName + "]";
	}
	
}
